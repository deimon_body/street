import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight, getDamage } from './fight';
import { showWinnerModal } from './modal/winner';
import { controls } from '../../constants/controls';
import { getComboAttack } from './fight';
import { changeHealthBar } from './fight';


var FIRST_HEALTH;
var SECOND_HEALTH;
var firstFighter;
var secondFighter;
const firstCombo = new Set();
const secondCombo = new Set(); 

export function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);
  root.innerHTML = '';
  root.append(arena);

  const [_firstFighter,_secondFighter] = selectedFighters; // write our fighters to variable
  firstFighter = _firstFighter;
  secondFighter = _secondFighter;

  firstFighter.hasComboAttempt = true;                   //give our fighters attemp to combo
  secondFighter.hasComboAttempt = true;
  FIRST_HEALTH = firstFighter.health;
  SECOND_HEALTH = secondFighter.health;

  document.addEventListener('keydown',keyDown);
  document.addEventListener('keyup',(e)=>{
    const keyBtn = e.code;
    const comand = Object.keys(controls).find(key => controls[key] === keyBtn);
    if(firstCombo.has(keyBtn)){
      console.log(keyBtn)
      firstCombo.delete(keyBtn)
    }

    if(secondCombo.has(keyBtn)){
      console.log(keyBtn)
      secondCombo.delete(keyBtn)
    }
    if(comand == 'PlayerOneBlock'){
      firstFighter.isInBlock = false;
    
    }
     else if (comand == 'PlayerTwoBlock'){
      secondFighter.isInBlock = false;
     
    }
  })
  callFightChecker(firstFighter,secondFighter);
}

export function keyDown(e){
  makePunch(e.code,firstFighter,secondFighter,FIRST_HEALTH,SECOND_HEALTH,firstCombo,secondCombo)
}
function makePunch(keyBtn,firstFighter,secondFighter,FIRST_HEALTH,SECOND_HEALTH,firstCombo,secondCombo){
  //if it`s combo attack:
  if(controls.PlayerOneCriticalHitCombination.includes(keyBtn)){
    firstCombo.add(keyBtn);
    makeCombo(firstCombo,controls.PlayerOneCriticalHitCombination,firstFighter,secondFighter,'right',SECOND_HEALTH);
    return
  }
  else if(controls.PlayerTwoCriticalHitCombination.includes(keyBtn)){
    secondCombo.add(keyBtn);
    makeCombo(secondCombo,controls.PlayerTwoCriticalHitCombination,secondFighter,firstFighter,'left',FIRST_HEALTH);
    return
  }


  //if just a simple attack;
  const comand = Object.keys(controls).find(key => controls[key] === keyBtn);
  
  if(comand == 'PlayerOneAttack'){
      let damage = getDamage(firstFighter,secondFighter,'right',SECOND_HEALTH);
      secondFighter.health-=damage;
      callFightChecker(firstFighter,secondFighter)
  }
  else if(comand == 'PlayerTwoAttack'){
      let damage = getDamage(secondFighter,firstFighter,'left',FIRST_HEALTH);
      firstFighter.health-=damage
      callFightChecker(firstFighter,secondFighter)
  }
    
  else if(comand == 'PlayerOneBlock'){
    firstFighter.isInBlock = true;
  }
   else if (comand == 'PlayerTwoBlock'){
    secondFighter.isInBlock = true;
  }
  
}

function makeCombo(setOfKeys,rightArr,attacker,defender,position,mainHealthVal){
  const isRightCombo = checkCombo(setOfKeys,rightArr);

  if(isRightCombo && attacker.hasComboAttempt){  //if attacker has attempt to do combo 
    setOfKeys.clear();
    attacker.hasComboAttempt = false;
    setTimeout(()=>{
      attacker.hasComboAttempt = true; 
      setOfKeys.clear();  
      console.log("Вы можете использовать комбо!!")   
    },10000)

    const damage = getComboAttack(attacker)  
    changeHealthBar(position,defender,mainHealthVal,damage)
    callFightChecker(attacker,defender);
    defender.health-=damage;
  }

}


function checkCombo(setOfKeys,rightArr){
  if(setOfKeys.size == 3){ //if the length is equal 3 which we need;
    for(let key of setOfKeys){  //if every key which was presse id right
      if(rightArr.indexOf(key) == -1){
        setOfKeys.clear();
        return false
      } 
    }
  }else{
    return false;
  } 
  return true
}

function callFightChecker(firstFighter,secondFighter){
  fight(firstFighter,secondFighter)
    .then((fighter)=>{
      showWinnerModal(fighter);
    })
}




function createArena(selectedFighters) {
  const [firstFighter,secondFighter] = selectedFighters;
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  

  

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  
  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}



