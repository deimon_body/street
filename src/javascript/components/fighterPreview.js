import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  if(fighter!=undefined){
    let fighterImg = createFighterImage(fighter);
    let fighterTitle = createFighterName(fighter);
    let fighterInfoBlock = createInfoBlock(fighter);
    let fighterInfoElements = [fighterImg,fighterTitle,fighterInfoBlock];
    
    fighterElement.append(...fighterInfoElements);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterName(fighter){
  
  const {name} = fighter;
  const nameEl = createElement({
    tagName:"p",
    className:"fighter-priview__title"
  })
  nameEl.textContent = name;
  return nameEl
}

function createInfoBlock(fighter){
    const {attack,health,defense} = fighter;
    const box = createElement({
      tagName:"div",
      className:"fighter-priview__box-info"
    })
    const healthEl = createElement({
      tagName:"div",
      className:"fighter-priview__info"
    }) 
      
    const attackEl = createElement({
      tagName:"div",
      className:"fighter-priview__info"
    }) 

    const defenseEl = createElement({
      tagName:"div",
      className:"fighter-priview__info"
    }) 

    healthEl.textContent = `Health:${health}`;
    attackEl.textContent = `Atack:${attack}`;
    defenseEl.textContent = `Defense:${defense}`;
    const blockInfo = [healthEl,attackEl,defenseEl];

    box.append(...blockInfo);
    return box
  }
